package assignment_1;

public class Question_1 {

	public static void main(String[] args) {
		int maxNum = Integer.MIN_VALUE;			// Taking minimum possible value of Integer in given compiler.
		for(String i : args) {
			if(maxNum < (Integer.parseInt(i))) {
				maxNum = Integer.parseInt(i);
			}
		}
		System.out.println(maxNum);
	}

}
