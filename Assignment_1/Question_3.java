package assignment_3;

import java.util.Scanner;

public class Question_3 {
	static int n1=0, n2=1, n3=0;
	
	public static void fibonacci(int count) {
		
		if(count > 0) {
			n3 = n2 + n1;
			n1 = n2;
			n2 = n3;
			
			System.out.print(" "+n3);
			fibonacci(count-1);
		}
		
		
	}

		public static void main(String[] args) {
			Scanner sc = new Scanner(System.in);
			System.out.println("Enter Count of elements needed in Fibonacci Series");
			System.out.println("Please enter a positive 'COUNT' ");
			int count = sc.nextInt();
			System.out.print((n1)+" "+n2);
			
			fibonacci(count-2);
			
			
			sc.close();
		}
		
}
