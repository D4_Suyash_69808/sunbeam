package assignment_2;

import java.util.Scanner;

public class Question_2 {
	
	//using recursion to calculate factorial
	public static int factorial(int num) {
		if(num < 0) // checking boundary condition for negative number input
			return 0;
		if(num == 0 || num ==1) 
			return 1;
		return num * factorial(num-1);
		}
	

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number to calculate its Factorial");
		int num = sc.nextInt();
		
		System.out.println("Factorial of given number is "+factorial(num));
		
		sc.close();
	}
	
 
}
